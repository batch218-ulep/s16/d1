console.log("Mabuhay");

// Arithmetic Operations

	let x = 200;
	let y = 18;

	console.log("x");
	console.log(x);
	console.log("y");
	console.log(y);

	console.log(' ');

	// Addition
	let sum = x + y;
	console.log('result of addition operator: ' + sum);

	// Subration
	let difference = x - y;
	console.log('result of subtraction operator: ' + difference);

	// Multiplication
	let product = x * y;
	console.log('result of subtraction operator: ' + product);

	// Division
	let qoutient = x / y;
	console.log('result of subtraction operator: ' + qoutient);

	//Remainder
	let remainder = x % y;
	console.log("Result of modulo operator: " + remainder);

	console.log(' ');

// Assignment Operator
	// Basic Assignment operator (=)
	// assign operator assigns the value of the reight hand operand to a variable
	let assignmentNumber = 8;
	console.log("The value of the assignmentNumber variable: " + assignmentNumber);
	console.log(' ');

// continuation of assignment operator

// Addition Assignment Opertaor
	// Syntax: assignmentNumber = assignmentNumber + 2; //long method

	assignmentNumber = assignmentNumber + 2;
	console.log(assignmentNumber);

	// Syntax: assignmentNumber +=2; //short method
	assignmentNumber +=2;
	console.log(assignmentNumber);

	// Subtraction/Multiplication/Division Assignment Operator (-=, *=, /=)
	assignmentNumber -= 3;
	console.log("Result of Subtraction assignment operator: " +assignmentNumber);

	assignmentNumber *= 2;
	console.log("Result of Multiplication assignment operator: " +assignmentNumber);

	assignmentNumber /= 2;
	console.log("Result of division assignment operator: " +assignmentNumber);

	console.log(" ");
	// Multiple Operators and Parentheses

	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log("Result of mdas operation: " + mdas);

	/*
		The operations were done in the following order
		1. 3 * 4 = 12 | 1 + 2 - 12 /5
		2. 12 / 5 = 2.4 | 1 + 2 - 2.4
		3. 1 + 2 = 3 | 3 - 2.4
		4. 3 - 2.4 = 0.6
	*/

	let pemdas = 1 + (2 - 3) * (4 / 5);
	console.log("Result of pemdas operation: " + pemdas);

	 //heirarchy
/* combinations of multiple arithmetic operators will follow the pemdas rule

	1. parenthesis
	2. exponent
	3. multiplication or division
	4. addition or subtraction
*/

	// Note: will also follow left to right rule

	console.log(' ');

	//incrementation and decrementation
	 // Operators that add or subtract values by 1 and reassigns the value of the variable where the increment(++)/decrement(--) was applied to

	let z = 1;

	//pre-incrementation
	let increment = ++z;
	console.log("Result of pre-increment: " + increment);
	console.log("Result of pre-increment of z: " + z);

	console.log(' ');

	//post-incrementation
	increment = z++;
	console.log("Result of post-incrmenet: " + increment);
	console.log("Result of post-increment of z: " + z);


	console.log('');

	//decrementation
	//pre-decremaentation
	let decrement = --z;
	console.log("Result of pre-decrement: " + decrement);
	console.log("Result of pre-decrement of z: " + z);

	console.log(' ');

	//post-decremaentation
	decrement = z--;
	console.log("Result of post-decrement: " + decrement);
	console.log("Result of pre-decrement of z: " + z);

	console.log(' ');

	//Type Coercion
	//Type coercion is the automatic or implicit conversion of values from one data type to another

	let numA = '10';
	let numB = 12;

	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);

	console.log(" ");

	let numC = 16;
	let numD = 14;

	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion);

	console.log(' ');

	//true = 1 | false = 1
	let numE = true + 1;
	console.log(numE);

	//false = 0
	let numF = false + 1;
	console.log(numF);

	console.log(' ')

	// combination of number and •olSÅrn Ota type will result to number
	let numX =10;
	let numY =true;
	// boolean values are
		// true = 1;
		// false = 0;
	coercion =
	numX + numY;
	console.log(coercion) ;
	console.log(typeof coercion);

	console.log(' ');

	// [SECTION] Comparison Operators
	let juan = "juan";

	//Equality Operator (==)
	// = Assignment operator
	// == Equality operator
	// === Strict operator

	console.log(1 == 1);
	console.log(1 == 2);
	console.log(1 == '1');
	console.log(0 == false);
	console.log("juan" == "juan");
	console.log("juan" == "Juan"); //Strict with letter casing
	console.log("juan" == juan);

	console.log(' ');

	//IneQuality Operator (!=)
	// ! = not

	console.log(1 != 1);
	console.log(1 != 2);
	console.log(1 != '1');
	console.log(0 != false); 
	console.log("juan" != "juan");
	console.log("juan" != "Juan"); 
	console.log("juan" != juan);

	console.log(' ');

	//Strict Equlity Operator (!==)
	  /* 
	            -Checks if the value or operand are equal are of the same type
	   */

	   console.log(1 === '1');
	   console.log('juan' === juan);
	   console.log(0 === false);

	console.log(' ');

	//Strict Inequality Operator

	console.log(1 !== '1');
	console.log('juan' !== juan);
	console.log(0 !== false);

	console.log(' ');

	//[SECTION] Relational Operator
	//Some comparison operators check whether one value is greater or less than to the other value.
	// >, <, =

	let a = 50;
	let b = 65;

	let isGreaterThan = a > b;
	console.log(isGreaterThan);

	let isLessThan = a < b;
	console.log(isLessThan);

	let isGTorEqual = a >= b;
	console.log(isGTorEqual);

	let isLTorEqual = a <= b;
	console.log(isLTorEqual);

	console.log(' ');

	// [SECTION] Logical Operators
	// && --> AND , || --> OR , ! --> NOT

	let isLegalAge = true;
	let isRegistered = false;

	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("Result of logical && Result: " + allRequirementsMet);

	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of logical OR Result: " + someRequirementsMet);

	let someRequirmentsNotMet = !isRegistered;
	console.log("Result of logical NOT Result: " + someRequirmentsNotMet);

